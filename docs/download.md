![Download LemonadeJS](img/download-lemonadejs.png)

Download
========

LemonadeJS is about 7 KBytes

LemonadeJS is a micro JavaScript library to help developers to build amazing reactive web-based applications. The library is free and distribute under MIT license, and you can use the library selecting one of the options below:



NPM
---

To install LemonadeJS using NPM

```bash
npm install lemonadejs
```


CDN
---
```html
<script src="https://cdn.jsdelivr.net/npm/lemonadejs@4/dist/lemonade.min.js"></script>
```


Website
-------
```html
<script src="https://lemonadejs.net/v4/lemonade.js"></script>
```


Download the library
--------------------

- [Version 1](https://lemonadejs.net/v1/lemonade.zip)
- [Version 2](https://lemonadejs.net/v2/lemonade.zip)
- [Version 3](https://lemonadejs.net/v4/lemonade.zip)
- [Version 4](https://lemonadejs.net/v4/lemonade.zip)