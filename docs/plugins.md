title: Dynamic Lists with LemonadeJS - Arrays and Loops Simplified
keywords: LemonadeJS, two-way data binding, frontend, javascript library, javascript plugin, javascript, reactive, react, lists, loops, arrays, plugins
description: Explore the LemonadeJS List component, a powerful library for creating dynamic elements with search and pagination from arrays of objects

![LemonadeJS Plugins](img/library.svg)

LemonadeJS Plugins
====================

On the website, you can find different official components. We can categorize those as Pico Extension, Plugins, and Integrations.


Pico Library
------------

A Pico library is a collection of particular components that follow some characteristics below.

*   It has less than 2 KBytes;
*   No dependencies required;
*   Highly optimized code;



### Available components

Currently, the following components are available on the official LemonadeJS Pico Library.

*   [List](/docs/plugins/list): Create a list of elements from an array based on a given template, including search and pagination.
*   [Rating](/docs/plugins/rating): A micro JavaScript star rating plugin.
*   [Router](/docs/plugins/router): Create a JavaScript single-page application with routes using LemonadeJS.
*   [Signature Pad](/docs/plugins/signature): A JavaScript Signature pad using LemonadeJS.



Components
----------

The LemonadeJS component library includes other JavaScript plugins and components for common requirements.

*   [Data Grid](/docs/plugins/data-grid): A micro (5KBytes) JavaScript Data Grid with search, pagination, sorting.
*   [Image Cropper](/docs/plugins/image-cropper): A linkedin-style photo cropper.



{.green}
> **Submit your components**
>
> If you have created something awesome and would like to share, please clone [https://github.com/lemonadejs/lemonadejs](https://github.com/lemonadejs/lemonadejs) and send your PR.
 
